import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:gallery_test_sp/models/Profile.dart';

class ImageScreen extends StatelessWidget{

  Profile profile;

  ImageScreen(Profile profile){
    this.profile = profile;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: new AppBar(title: Text('Image view')),
        body: Stack(
          children: <Widget>[
            Center(child: CircularProgressIndicator()),
            Center(child: FadeInImage.assetNetwork(
                placeholder: 'assets/images/loader_b.png',
                image: profile.urls.full),)
          ],
        ),
//      body: Center(
//        child: Image.network(profile.urls.full, scale: 1,),)

    );
  }

}