// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Urls.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Urls _$UrlsFromJson(Map<String, dynamic> json) {
  return Urls(
      json['raw'] as String,
      json['full'] as String,
      json['regular'] as String,
      json['small'] as String,
      json['thumb'] as String);
}

Map<String, dynamic> _$UrlsToJson(Urls instance) => <String, dynamic>{
      'raw': instance.raw,
      'full': instance.full,
      'regular': instance.regular,
      'small': instance.small,
      'thumb': instance.thumb
    };
