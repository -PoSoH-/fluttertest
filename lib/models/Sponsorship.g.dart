// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Sponsorship.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Sponsorship _$SponsorshipFromJson(Map<String, dynamic> json) {
  return Sponsorship(
      json['impressionsId'] as String,
      json['tagline'] as String,
      json['sponsor'] == null
          ? null
          : Sponsor.fromJson(json['sponsor'] as Map<String, dynamic>));
}

Map<String, dynamic> _$SponsorshipToJson(Sponsorship instance) =>
    <String, dynamic>{
      'impressionsId': instance.impressionsId,
      'tagline': instance.tagline,
      'sponsor': instance.sponsor
    };
