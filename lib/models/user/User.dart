import 'package:json_annotation/json_annotation.dart';
import 'Links.dart';
import '../ProfileImage.dart';

part 'User.g.dart';

@JsonSerializable()
class User {

  String id;
  String updatedAt;
  String username;
  String name;
  String firstName;
  String lastName;
  String twitterUsername;
  String portfolioUrl;
  String bio;
  String location;
  Links links;
  ProfileImage profileImage;
  String instagramUsername;
  int totalCollections;
  int totalLikes;
  int totalPhotos;
  int acceptedTos;

  User(this.id,
      this.updatedAt,
      this.username,
      this.name,
      this.firstName,
      this.lastName,
      this.twitterUsername,
      this.portfolioUrl,
      this.bio,
      this.location,
      this.links,
      this.profileImage,
      this.instagramUsername,
      this.totalCollections,
      this.totalLikes,
      this.totalPhotos,
      this.acceptedTos);

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
  Map<String, dynamic> toJson() => _$UserToJson(this);

  static User parseMapToUser(Map user){

    Map links = user['links'];
    Map image = user['profile_image'];

    Links linksObj = new Links(links['self']
        , links['html']
        , links['photos']
        , links['likes']
        , links['portfolio']
        , links['following']
        , links['followers']);

    ProfileImage imageObj = new ProfileImage(image['small']
        , image['medium']
        , image['large']);

    User userObj  = new User(user['id']
        , user['updatedAt']
        , user['username']
        , user['name']
        , user['firstName']
        , user['lastName']
        , user['twitterUsername']
        , user['portfolioUrl']
        , user['bio']
        , user['location']
        , linksObj // user['links']
        , imageObj //user['profileImage']
        , user['instagramUsername']
        , user['totalCollections']
        , user['totalLikes']
        , user['totalPhotos']
        , user['acceptedTos']);

    return userObj;
  }

}
