import 'package:gallery_test_sp/models/sponsor/Links.dart';
import 'package:json_annotation/json_annotation.dart';
import 'sponsor/Sponsor.dart';

part 'Sponsorship.g.dart';

@JsonSerializable()
class Sponsorship {
  String impressionsId;
  String tagline;
  Sponsor sponsor;

  Sponsorship(this.impressionsId, this.tagline, this.sponsor);

  factory Sponsorship.fromJson(Map<String, dynamic> json) =>
      _$SponsorshipFromJson(json);

  Map<String, dynamic> toJson() => _$SponsorshipToJson(this);

  static Sponsorship parseMapToObj(Map spMap){

    if(spMap == null) return null;

    Map sponsor = spMap['sponsor'];
    Map links = sponsor['links'];
    Links linksObj = new Links(links['self']
        , links['html']
        , links['photos']
        , links['likes']
        , links['portfolio']
        , links['following']
        , links['followers']);

    Sponsor spObj = new Sponsor(sponsor['id']
        , sponsor['updatedAt']
        , sponsor['username']
        , sponsor['name']
        , sponsor['firstName']
        , sponsor['lastName']
        , sponsor['twitterUsername']
        , sponsor['portfolioUrl']
        , sponsor['bio']
        , sponsor['location']
        , linksObj //sponsor['links']
        , sponsor['profileImage']
        , sponsor['instagramUsername']
        , sponsor['totalCollections']
        , sponsor['totalLikes']
        , sponsor['totalPhotos']
        , sponsor['acceptedTos']);

    Sponsorship spipObj = new Sponsorship(spMap['impressionsId']
        , spMap['tagline'], spObj);

    return spipObj;
  }

}
