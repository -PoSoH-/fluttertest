import 'package:json_annotation/json_annotation.dart';

part 'Urls.g.dart';

@JsonSerializable()
class Urls {
  String raw;
  String full;
  String regular;
  String small;
  String thumb;

  Urls(this.raw, this.full, this.regular, this.small, this.thumb);

  factory Urls.fromJson(Map<String, dynamic> json) => _$UrlsFromJson(json);

  Map<String, dynamic> toJson() => _$UrlsToJson(this);
}
