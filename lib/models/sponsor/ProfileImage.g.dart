// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ProfileImage.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProfileImage _$ProfileImageFromJson(Map<String, dynamic> json) {
  return ProfileImage(json['small'] as String, json['medium'] as String,
      json['large'] as String);
}

Map<String, dynamic> _$ProfileImageToJson(ProfileImage instance) =>
    <String, dynamic>{
      'small': instance.small,
      'medium': instance.medium,
      'large': instance.large
    };
