// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Sponsor.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Sponsor _$SponsorFromJson(Map<String, dynamic> json) {
  return Sponsor(
      json['id'] as String,
      json['updatedAt'] as String,
      json['username'] as String,
      json['name'] as String,
      json['firstName'] as String,
      json['lastName'] as String,
      json['twitterUsername'] as String,
      json['portfolioUrl'] as String,
      json['bio'] as String,
      json['location'] as String,
      json['links'] == null
          ? null
          : Links.fromJson(json['links'] as Map<String, dynamic>),
      json['profileImage'] == null
          ? null
          : ProfileImage.fromJson(json['profileImage'] as Map<String, dynamic>),
      json['instagramUsername'] as String,
      json['totalCollections'] as int,
      json['totalLikes'] as int,
      json['totalPhotos'] as int,
      json['acceptedTos'] as bool);
}

Map<String, dynamic> _$SponsorToJson(Sponsor instance) => <String, dynamic>{
      'id': instance.id,
      'updatedAt': instance.updatedAt,
      'username': instance.username,
      'name': instance.name,
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'twitterUsername': instance.twitterUsername,
      'portfolioUrl': instance.portfolioUrl,
      'bio': instance.bio,
      'location': instance.location,
      'links': instance.links,
      'profileImage': instance.profileImage,
      'instagramUsername': instance.instagramUsername,
      'totalCollections': instance.totalCollections,
      'totalLikes': instance.totalLikes,
      'totalPhotos': instance.totalPhotos,
      'acceptedTos': instance.acceptedTos
    };
