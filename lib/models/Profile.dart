import 'package:json_annotation/json_annotation.dart';

import 'Links.dart';
import 'Sponsorship.dart';
import 'Urls.dart';
import 'user/User.dart';

part 'Profile.g.dart';

@JsonSerializable()
class Profile {
  String id;
  String createdAt;
  String updatedAt;
  int width;
  int height;
  String color;
  String description;
  Object altDescription;
  Urls urls;
  Links links;
  List<Object> categories = null;
  int likes;
  bool likedByUser;
  List<Object> currentUserCollections = null;
  User user;
  Sponsorship sponsorship;

  Profile(
      this.id,
      this.createdAt,
      this.updatedAt,
      this.width,
      this.height,
      this.color,
      this.description,
      this.altDescription,
      this.urls,
      this.links,
      this.categories,
      this.likes,
      this.likedByUser,
      this.currentUserCollections,
      this.user,
      this.sponsorship);

  factory Profile.fromJson(Map<String, dynamic> json) =>
      _$ProfileFromJson(json);

  Map<String, dynamic> toJson() => _$ProfileToJson(this);
}
