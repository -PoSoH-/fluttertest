import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gallery_test_sp/ImageScreen.dart';
import 'package:gallery_test_sp/MainBody.dart';
import 'package:gallery_test_sp/models/Links.dart';
import 'package:gallery_test_sp/models/Sponsorship.dart';


import 'package:http/http.dart' as http;
import 'dart:convert';

import 'models/Profile.dart';
import 'models/Urls.dart';
import 'models/user/User.dart';


class MainBodyState extends State<MainBody> {
  List<Profile> information = new List();

  @override
  Widget build(BuildContext context) {
    var futureBuilder = new FutureBuilder(
      future: _getData(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
          case ConnectionState.waiting:
            return new Column(
                children: <Widget>[
                  new Text('loading...', style: TextStyle(fontSize: 20, color: Colors.blue),),
                  new Image.asset('asset/images/loader.gif')
                ]);
          default:
            if (snapshot.hasError)
              return new Text('Error: ${snapshot.error}',
                style: TextStyle(fontSize: 20, color: Colors.red),);
            else
              return createListView(context, snapshot);
        }
      },
    );


    return new Scaffold(
      body: futureBuilder,
    );
  }

  Widget createListView(BuildContext context, AsyncSnapshot snapshot) {
    List<Profile> values = snapshot.data;
    return new ListView.builder(
      itemCount: values.length,
      itemBuilder: (BuildContext context, int index) {
        return new Column(
          children: <Widget>[
            new Row(
              children: <Widget>[
                new GestureDetector(
                  onTap: (){
                    print('start new view $index');
                    _toNextScreen(context, index);
                  },
                  child: new FadeInImage.assetNetwork(
                      placeholder: 'assets/images/loader_b.png',
                      image: values[index].urls.small, height: 100, width: 100,),
                ) ,

                new Column(children: <Widget>[
                  new Align(
                    alignment: Alignment(-1,0),
                    child: new Text(
                      values[index].user.name,
                      style: TextStyle(fontSize: 18,),
                      textAlign: TextAlign.start,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),

                  new Align(
                    alignment: Alignment(-1, 0),
                    child: new Text(
                      values[index].user.bio == null ? values[index].user.name
                              : values[index].user.bio,
                      textAlign: TextAlign.start,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ],)
              ]
            ),
//          ]  new ListTile(
            new Divider(height: 2.0,),
          ],
        );
      },
    );
  }

  Future<List<Profile>> _getData() async {
    List<dynamic> profiles = new List();
    await http.get('https://api.unsplash.com/photos/?client_id=cf49c08b444ff4cb9e4d126b7e9f7513ba1ee58de7906e4360afc1a33d1bf4c0')
        .then((response) {
          if(response.statusCode == 200) {
            print(response.statusCode);
            String temp = response.body;
            profiles.addAll(jsonDecode(response.body));
            print(profiles.length);
          }else{
            print('error $response.statusCode');
          }

    }).catchError((onError){
      print(onError);
    });

    var values = addData(profiles);

    return values;
  }

  List<Profile> addData(List<dynamic> data) {

    dynamic tmp;
    Map map;
    for(int i = 0; i < data.length; i++){

      tmp = data.elementAt(i);
      map = tmp;

      print(map['id']);
      print(map['created_at']);

      Map urls = data[i]['urls'];
      Map links = data[i]['links'];

      Urls urlsObj = new Urls(urls['raw']
          , urls['full'], urls['regular'], urls['small'], urls['thumb']);
      Links linksObj = new Links(links['self'], links['html'], links['download']
          , links['downloadLocation']);
      User userObj = User.parseMapToUser(data[i]['user']);

      Sponsorship ship = Sponsorship.parseMapToObj(data[i]['sponsorship']);

      information.add(new Profile(
          data[i]['id'],
          data[i]['createdAt'],
          data[i]['updatedAt'],
          data[i]['width'],
          data[i]['height'],
          data[i]['color'],
          data[i]['description'],
          data[i]['altDescription'],
          urlsObj,
          linksObj,
          data[i]['categories'],
          data[i]['likes'],
          data[i]['likedByUser'],
          data[i]['currentUserCollections'],
          userObj, //data[i]['user'],
          ship /*data[i]['sponsorship']*/));

      print(information.length);
    }
    print(information.length);
    return information;
  }

  void _toNextScreen(BuildContext context, int id){
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => ImageScreen(information[id]))); //, information[id]);
  }

}
