import 'package:flutter/material.dart';

import 'MainBodyState.dart';

class MainScreen extends StatelessWidget{

  MainBodyState _mainBody = new MainBodyState();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: new Scaffold(
          appBar: new AppBar(
              title: new Text('List profile')),
          body: _mainBody.build(context)
      ),
    );
  }

  void setValues(List<dynamic> info){

    _mainBody.addData(info);

  }

}